import React, { Component } from 'react';
import './Home.css';
// import TopImg from '../../Images/HomeTop.png';
// import SecBImg from '../../Images/Sec2Img.png';
import { Link } from 'react-router-dom';
import CurCommImg from '../../Images/Cur_Comm_img.png';
import EngCus from '../../Images/Eng_cus.png';
import SpkCha from '../../Images/Spk_Cha.png';
import PutEvr from '../../Images/Put_Evr.png';
import GhIconA from '../../Images/GhIconA.svg';
import GhIconB from '../../Images/GhIconB.svg';
import GhIconC from '../../Images/GhIconC.svg';
import CodeChImg from '../../Images/codeCh_img.png';
import MyselfIcon from '../../Images/mselfIcon.svg';
import CompanyIcon from '../../Images/CompanyIcon.png';
import NonProfitIcon from '../../Images/NonProfitIcon.svg';
// import JobIcon from '../../Images/JobIcon.svg';
import { ProductConsumer } from '../../Context_Api/Context';
// import Footer from '../Footer/Footer';

export default class Home extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { menuFullDrop, TopmenuFullClose, homeHeadName, homeParaName, forMyselfVersion, forMyCompanyVersion,
                        forMyNonprofitVersion, menuVersionName } = value;
                    return (
                        <>
                            {menuFullDrop ?
                                <div className="full-vr-sel-fix">
                                    <div className="close-ver-sel">
                                        <i className="fas fa-times-circle" onClick={TopmenuFullClose} />
                                    </div>
                                    <h1>I Am Interested In Government Programs</h1>
                                    <div className="ver-sel-menu">
                                        <div onClick={forMyselfVersion}>
                                            <img src={MyselfIcon} width="40px" alt="no_img" />
                                            <p>For Myself</p>
                                        </div>
                                        <div onClick={forMyCompanyVersion}>
                                            <img src={CompanyIcon} width="40px" alt="no_img" />
                                            <p>For My Company</p>
                                        </div>
                                        {/* <div onClick={forMyJobVersion}>
                                        <img src={JobIcon} width="40px" alt="no_img" />
                                        <p>For My Job</p>
                                    </div> */}
                                        <div onClick={forMyNonprofitVersion}>
                                            <img src={NonProfitIcon} width="40px" alt="no_img" />
                                            <p>For My Non-Profit</p>
                                        </div>
                                    </div>
                                </div>
                                : null}
                            <div>
                                <div className="full-tp-home">
                                    <div className="home-of-top">
                                        <h1>{homeHeadName}</h1>
                                        <p style={{marginBottom:"6%"}}>{homeParaName}</p>
                                        {menuVersionName === 'For Businesses' ?
                                            <div>
                                                <Link to="/trending" className="btn-onhold-trans btn mr-1 w-resp-bt">All Programs</Link>
                                                <button className="btn-onhold-trans btn ml-1 w-resp-bt">Get Started</button>
                                            </div>
                                            : null}
                                        {menuVersionName === 'For Individuals' || menuVersionName === 'For NGO’s' || menuVersionName === 'I’m A Non-Profit' ?
                                            <div>
                                                <Link to="/trending" className="btn-onhold-trans btn mr-1 w-resp-bt">All Programs</Link>
                                                <button className="btn-onhold-trans btn ml-1 w-resp-bt">Get Started</button>
                                            </div>
                                            : null}
                                    </div>
                                    <Link to="/trending">
                                    <div className="see-more-tag">
                                        <p>Learn About The Amazon Covid Relief Benefits You Can Get</p>
                                        <div className="d-flex flex-column">
                                            <i className="fas fa-angle-down arr-ani" style={{ textAlign: "center" }} />
                                            <i className="fas fa-angle-down arr-ani" style={{ textAlign: "center" }} />
                                            <i className="fas fa-angle-down arr-ani" style={{ textAlign: "center" }} />
                                        </div>
                                    </div>
                                    </Link>
                                </div>
                                <div className="full-tp-home-mob">
                                    <div style={{marginBottom: "14%"}}>
                                        <h1 style={{ fontWeight: "700" }}>Canada’s</h1>
                                        <p className="mb-4">Government Programs</p>
                                    </div>
                                    <div className="d-flex align-item-center flex-column">
                                        <Link to="/trending" className="btn-onhold-trans btn w-resp-bt my-2">All Programs</Link>
                                        <button className="btn-onhold-trans btn w-resp-bt my-2">Get Started</button>
                                    </div>
                                    {/* <Link to="/trending"> */}
                                    <div className="mob-bt-tg">
                                        <p>Learn About Covid Refiel</p>
                                    </div>
                                    {/* </Link> */}
                                </div>
                                {/* <div className="d-flex justify-content-center"><img src={SecBImg} style={{ padding: "32px 16px", width: "100%" }} alt="no_img" /></div> */}
                                <div className="container">
                                    <div className="comm-cur">
                                        <div className="com-left-cur">
                                            <small>INBOX</small>
                                            <h2>Centralize your customer communication</h2>
                                            <p>Deliver simple, seamless customer support. Unify and track interactions across every messaging channel for more efficient processes and better customer experiences.</p>
                                            <Link to="/">Learn more about Inbox</Link>
                                        </div>
                                        <div className="com-right-cur">
                                            <img src={CurCommImg} alt="no_img" style={{ width: "100%" }} />
                                        </div>
                                    </div>
                                </div>
                                <div className="eng-at">
                                    <div className="container">
                                        <div className="eng-at-cous">
                                            <div className="eng-at-right-cous">
                                                <img src={EngCus} alt="no_img" style={{ width: "100%" }} />
                                            </div>
                                            <div className="eng-at-right-cous">
                                                <small>FLOW BUILDER</small>
                                                <h2>Engage customers through seamless automation</h2>
                                                <p>Create modern communication experiences that drive faster interactions, higher engagement, and more efficient processes without writing a single line of code.</p>
                                                <Link to="/">Learn more about Flow Builder</Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="container">
                                    <div className="comm-cur">
                                        <div className="com-left-cur">
                                            <small>DEVELOPER APIS</small>
                                            <h2>Deliver programmatic communications at scale</h2>
                                            <p>Build voice, SMS, WhatsApp, Messenger, WeChat and more directly into your application. Programmatically verify users, buy and use phone numbers, and manage contacts through REST APIs.</p>
                                            <Link to="/">Learn more about our APIs</Link>
                                        </div>
                                        <div className="com-right-cur">
                                            <img src={CodeChImg} alt="no_img" style={{ width: "100%" }} />
                                        </div>
                                    </div>
                                </div>
                                <div className="container">
                                    <div className="eng-at-cous" style={{ padding: "100px 32px" }}>
                                        <div className="eng-at-right-cous">
                                            <img src={SpkCha} alt="no_img" style={{ width: "100%" }} />
                                        </div>
                                        <div className="eng-at-right-cous">
                                            <small>CHANNELS</small>
                                            <h2 style={{ color: "#000" }}>Speak and be heard. Read and respond. On every channel.</h2>
                                            <p>Access every communication channel your customers use and love. Optimize your communications for reliability, quality, and cost, through a single API.</p>
                                            <Link to="/">Learn more about our Channels</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="container">
                                    <div className="comm-cur">
                                        <div className="com-left-cur">
                                            <small>INSIGHTS & REPORTING</small>
                                            <h2>Put your data to work, everywhere</h2>
                                            <p>Share context across your business by building data pipelines between disparate systems with simple webhooks and drag-and-drop programming.</p>
                                        </div>
                                        <div className="com-right-cur">
                                            <img src={PutEvr} alt="no_img" style={{ width: "100%" }} />
                                        </div>
                                    </div>
                                </div>
                                <div className="eng-at">
                                    <div className="container">
                                        <div className="eng-at-cous">
                                            <div className="eng-at-right-cous">
                                                <p style={{ fontSize: "20px", color: "#fff" }}>In the past, we would be lucky to send 1M messages in 4-5 hours. With MessageBird, we send 1 M messages in 10 minutes. Every second counts, MessageBird is literally saving lives.</p>
                                                <p>Frank Hoen</p>
                                            </div>
                                            <div className="eng-at-right-cous">
                                                <img src={EngCus} alt="no_img" style={{ width: "100%" }} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style={{ padding: "100px 0" }}>
                                    <div className="container">
                                        <h2 className="mb-5 text-center">You're in good hands</h2>
                                        <div className="row">
                                            <div className="col-md-4">
                                                <h5><img src={GhIconA} alt="no_img" /> Secure by design</h5>
                                                <p>Enterprise-grade security is built into our products by default.</p>
                                            </div>
                                            <div className="col-md-4">
                                                <h5><img src={GhIconB} alt="no_img" /> World-class support</h5>
                                                <p>24/7 support for you and your team, with MessageBird offices in 7 countries.</p>
                                            </div>
                                            <div className="col-md-4">
                                                <h5><img src={GhIconC} alt="no_img" /> Regulatory compliance</h5>
                                                <p>Built for international laws and regulations, including GDPR and PSD2.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* <div style={{ padding: "100px 0" }}>
                                    <div className="container text-center">
                                        <h1>Start offering your customers a better <br /> support experience</h1>
                                        <div className="mt-5">
                                            <button className="btn mr-1" style={{ backgroundColor: "#0089ff", color: "#fff", minWidth: "160px" }}>Use for free</button>
                                            <button className="btn ml-1" style={{ backgroundColor: "#fff", color: "#0089ff", minWidth: "160px", border: "1px solid #e5efff" }}>Talk to sales</button>
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                         {/* <Footer/> */}
                        </>
                    )
                })}
            </ProductConsumer>
        )
    }
}