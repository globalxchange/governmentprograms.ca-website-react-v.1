import React, { Component } from 'react';
import './Footer.css';
import MenuIconA from '../../Images/menu-icon-inbox.svg';

export default class Footer extends Component {
    render() {
        return (
            <div className="container">
                <div className="d-flex justify-content-center py-3 flex-wrap">
                    <h6 className="foot-menus">About</h6>
                    <h6 className="foot-menus">Privacy</h6>
                    <h6 className="foot-menus">Legal</h6>
                    <h6 className="foot-menus">Security</h6>
                    <h6 className="foot-menus">Developers</h6>
                    <h6 className="foot-menus">Support</h6>
                    <h6 className="foot-menus">Careers</h6>
                </div>
                <div className="text-center pb-3" style={{color:"#9fa7b2"}}><small>© 2020 OnHold All rights reserved.</small></div>
                <div className="text-center pb-3"><img src={MenuIconA} width="50px" alt="no_img" /></div>
            </div>
        )
    }
}