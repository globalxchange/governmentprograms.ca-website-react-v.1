import React, { Component, useState, useEffect } from 'react';
import './Trending.css';
import ProgramImg from '../../Images/Program_b.svg';
import Government_p from '../../Images/Government_p.svg'
import axios from "axios"
import { message, Modal, Button } from 'antd';


function Trending(props) {
    const [articles, setarticles] = useState("")
    const [title, settitle] = useState("")
    const [body, setbody] = useState("")
    const [photo1, setphoto] = useState("")
    const ABC = <div className="prg-def-cors">CEBA</div>
    useEffect(() => {
        axios.get('https://counsel.apimachine.com/api/getlawarticlebylegalprofessional/5ef1e932bb42614561a01d89')
            .then(response => {
                if (response.data.success) {
                    // setTimeout(getarticles, 100);
                    setarticles(response.data.data)

                } else {
                    message.error(response.data.message)
                }
            }).catch(err => console.log(err))
    }, [])
    console.log("cover-photo" + JSON.stringify(articles))
    if (articles.length == 0) {
        return (
            <>
                {message.loading("")}
            </>
        )
    }
    // if (articles.length != 0) {
    //     settitle(articles[0].Title)
    //     setbody(articles[0].Body)
    // }
    const theimage = (e)=>{
        setphoto(e.target.src)
        console.log((photo1) + " photoos")
        articles.forEach(element => {
            if(element.CoverPhoto === e.target.src){
                console.log(element.Body + ' Body--')
                setbody(element.Body)
                settitle(element.Title)
            }
        });
    }
     if(title === ""){
         settitle(articles[0].Title)
     }
     if(body === ""){
         setbody(articles[0].Body)
     }
    return (
        <div className="full-prg">
            <div className="prg-crs">
                {articles.map(photo => {
                    return  <input className = "thedisp" onClick = {theimage} type = "image" src={photo.CoverPhoto} style={{ minWidth: "20vw", minHeight: "20vh", maxHeight: "220px" }} alt="no_image" />
                })}
                {/* {ABC}{ABC}{ABC}{ABC}{ABC}{ABC} */}
            </div>
            <div className="trnd-cont">
                <div className="row">
                    <div className="col-8">
                        <h2>{title}</h2>
                        <p style={{ textAlign: "justify" }}>{body}</p>
                    </div>
                    <div className="pr-0 col-4">
                        <div className="bt-comb">
                            <div className="btns-t">
                                <span className="mr-3">Apply With</span> <img src={Government_p} alt="no_image" />
                            </div>
                            <div>
                                <div className="bt-grs">Not Interested In Applying? We Can Still Get You Paid</div>
                                <div className="btns-t-not">
                                    Sell You Inactive Business <br />Instantly,
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Trending;
