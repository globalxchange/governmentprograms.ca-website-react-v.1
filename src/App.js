import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './Components/Home/Home';
import NavbarTop from './Components/NavbarTop/NavbarTop';
import Trending from './Components/Trending/Trending';

function App() {
  return (
    <div>
      <NavbarTop/>
      <Router>
        <Route exact path="/" component={props => <Home />} />
        <Route exact path="/trending" component={props => <Trending />} />
      </Router>
    </div>
  );
}

export default App;
